let countDownDate = new Date("Jun 21, 2020 20:00:00").getTime();

// Update the count down every 1 second
let x = setInterval(function () {
    // Get today's date and time
    let now = new Date().getTime();
    // Find the distance between now and the count down date
    let distance = countDownDate - now;
    // Time calculations for days, hours, minutes and seconds
    let jours = Math.floor(distance / (1000 * 60 * 60 * 24));
    let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    let seconds = Math.floor((distance % (1000 * 60)) / 1000);
    // Output the result in an element with id="compte-a-rebour"
    document.getElementById("compte-a-rebour").innerHTML = jours + "d " + hours + "h "
        + minutes + "m " + seconds + "s pour bénéficier d’une remise de -10%";
    // If the count down is over, write some text 
    if (distance < 0) {
        clearInterval(x);
        document.getElementById("compte-a-rebour").innerHTML = "Action est terminée";

    }
    // S'il reste moins d'un jour ajoute la classe rouge
    if (jours < 1) {
        clearInterval(x);
        let rouge = document.getElementById("compte-a-rebour");
        rouge.classList.add('rouge')

    }
}, 1000);

