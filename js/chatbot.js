let formulaire = document.getElementById('messageForm');
formulaire.addEventListener('submit', () => getMessage(event));

var questionList = [
    ['question', 'réponse'],
    ['bonjour', 'Bonjour, comment ca va ?'],
    ['ça va ?', 'Ca va et toi ?'],
    ['tu fais quoi ?', 'Rien'],
    ['ludo', 'le meilleur on regole o quoi?'],
    ['diogo', 'monsier Audi TT'],
    ['mamma', ' ciao franco che fai'],
    ['papa', ' è a lavoro'],
    ['mattia', 'mattia deve andare a letto'],
    ['no', 'mangia cioccolato'],
    ['merci', 'bonne nuit papa']


];

getMessage = (event) => {
    event.preventDefault();

    let message = document.getElementById('message');
    ecrireText(message.value, 'messageUser');

    var time = setTimeout(setAnswer(message.value.toLowerCase()), 2000);

    document.getElementById('message').value = ' ';
}

setAnswer = (message) => {

    message = message.replace(/\s+/g, "").replace(/[.,/#!$%^&*;:{}=-_`~()]/g, "");
    let chatbot = document.getElementById('chatbot');

    if (getAnswer(message) == false) {
        let error = "ERROR!!!!";
        ecrireText(error, 'error');
        chatbot.scrollTop = chatbot.scrollHeight;
    } else {
        let answer = getAnswer(message);
        ecrireText(answer, 'messageBot');
    }
}

getAnswer = (question) => {

    let reponse;
    let find = false;

    question = question.trim();
    questionList.forEach(questionReponse => {
        if (questionReponse[0] == question) {
            reponse = questionReponse[1];
            find = true;
        }
    });

    if (find == false) {
        reponse = false;
    }

    return reponse;
}

ecrireText = (message, className) => {

    let chatbot = document.getElementById('chatbot');

    let divMessage = document.createElement('div');
    divMessage.setAttribute('class', className);

    chatbot.appendChild(divMessage);
    let txtMessage = document.createTextNode(message);
    divMessage.appendChild(txtMessage);

}

let btn = document.getElementById('close');
btn.addEventListener('click', () => closeModale());
let btn2 = document.getElementById('open');
btn2.addEventListener('click', () => closeModale());


closeModale = () => {

    let modaleNewsletter = document.querySelector('.mise');
    modaleNewsletter.classList.toggle('display')
}

/*
let chatClose = document.querySelector('.material-icons');
chatClose.addEventListener('click',() => {
    console.log(chatbox);
    chatbox.classList.add('display');
});
*/
