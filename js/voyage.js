class Voyage {

    //mise en place du constructeur
    constructor(destination, voyageurs, dateDepart, dateRetour) {
        this.destination = destination;
        this.dateDepart = dateDepart;
        this.dateRetour = dateRetour;
        this.voyageurs = voyageurs;

    }

    //Méthode = fonction 
    displayVoyage() {
        let textVoyageur = "";
        for (let i = 0; i < this.voyageurs.length; i++) {

            textVoyageur += this.voyageurs[i].displayVoyageur();
        }
        return `${textVoyageur} '\n' Depart pour:'\n' ${this.destination} le ${this.dateDepart} '\n' e fait retour le ${this.dateRetour}`;
    }
}