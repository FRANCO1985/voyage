/*let pays = ['Italie', 'Suisse', 'France', 'Espagne'];*/
let jours = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];
let mois = ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre'];
let voyageurs = [];


//-------------------------------------formulaire donne personeel----//
let container = document.getElementById("DonneePersonnels");
function OnPartecipantChange() {
    container.innerHTML = " ";
    let partecipant = document.getElementById("partecipant").value;
    //container.appendChild(document.createTextNode("Formulaire pour"+ nbrPersonne + "personne"));

    for (let i = 0; i < partecipant; i++) {
        let p = document.createElement("p");
        container.appendChild(p);

        p.appendChild(document.createTextNode("Personne" + parseInt(i + 1)));

        //containerGroupForm
        let containerGroupForm = document.createElement("div");
        containerGroupForm.className = "form-group";
        container.appendChild(containerGroupForm);

        //input pour le nom
        let inputNom = document.createElement("input");
        inputNom.type = "text";
        inputNom.name = "NomPersonne" + parseInt(i + 1);
        inputNom.id = "NomPersonne" + parseInt(i + 1);
        inputNom.placeholder = "Votre nom";
        //inputNom.required = true;
        containerGroupForm.appendChild(inputNom);

        // input pour le prenom
        let inputPrenom = document.createElement("input");
        inputPrenom.type = "text";
        inputPrenom.name = "PrenomPersonne" + parseInt(i + 1);
        inputPrenom.id = "PrenomPersonne"  + parseInt(i + 1);
        inputPrenom.placeholder = "Votre prenom";
        //inputPrenom.required = true;
        containerGroupForm.appendChild(inputPrenom);

        // date de naissance
        let inputDateNaissance = document.createElement("input");
        inputDateNaissance.type = "date";
        inputDateNaissance.name = "DateNaissance" + parseInt(i + 1);
        inputDateNaissance.id = "DateNaissance"  + parseInt(i + 1);
        //inputDateNaissance.required = true;
        containerGroupForm.appendChild(inputDateNaissance);

    }
}



let monFormulaire = document.getElementById('monFormulaire');

//---------validation de les champs formulaire----------//

validation = (event) => {
    let error = "";
    //recuperer tout les champs
    
    let destination = document.getElementById('destination').value; 
    let date_de_aller = document.getElementById('date-de-aller').value;
    //console.log(destination);
    let date_de_retour = document.getElementById('date-de-retour').value;
    let partecipant = document.getElementById('partecipant').value;

    event.preventDefault();
    // verificaiton de la destination
    if (destination == '') {
        error += " la destination est vide ";

        //faire une classe dans le css pour l'error
       // document.getElementById('destination').style.backgroundColor = "red";
       // document.getElementById('destination').style.color = "#FFF";  
    } if (date_de_aller == '') {
        error += 'data de aller c est pas ok ';
        //  document.getElementById('date-de-aller').style.backgroundColor = "red";
        //  document.getElementById('date-de-aller').style.color = "#FFF";
    } if (date_de_retour == '') {
        error += ' data  de retour c est  pas ok ';
        // document.getElementById('date-de-retour').style.backgroundColor = "red";
        //  document.getElementById('date-de-retour').style.color = "#FFF";
    } if (date_de_retour < date_de_aller) {
        error += 'attention la date de retour il peu pas etre anteriore de la date de aller';
        // document.getElementById('date-de-retour').style.backgroundColor = "red";
        // document.getElementById('date-de-retour').style.color = "#FFF";
    } if (partecipant == '') {
        error += 'controller le numero de personne ';
        // document.getElementById('NbrPersonne').style.backgroundColor = "red";
        // document.getElementById('NbrPersonne').style.color = "#FFF";
    } else {
        for (let i = 0; i < partecipant; i++) {
            let inputName = document.getElementById("NomPersonne" + parseInt(i + 1));
            if (inputName.value == "") {
                error += "le nom est vide ";
            }
            let inputPrenom = document.getElementById("PrenomPersonne"  + parseInt(i + 1));
            if (inputPrenom.value == "") {
                error += "prenom est vide ";
            }
            let inputDateNaissance = document.getElementById("DateNaissance" + parseInt(i + 1));
            if (inputDateNaissance.value == "") {
                error += "datenaissance vide ";
            }

            let voyageur = new Voyageur(inputName.value, inputPrenom.value, inputDateNaissance.value);
            voyageurs.push(voyageur);
        }
        // alert('Votre donnees sont bien registrè');
    }
    if (error == "") {
         let voyage = new Voyage(destination, voyageurs, date_de_aller, date_de_retour);
         alert(voyage.displayVoyage());

    } else {
        alert(error);
        return false;
    }
}

monFormulaire.addEventListener('submit', validation);


OnPartecipantChange();



